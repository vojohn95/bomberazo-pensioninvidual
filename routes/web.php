<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('bombas.index');
});*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/verificar', 'BombaController@Carga')->name('verificar');

Route::get('/timbrado', 'BombaController@timbrado')->name('timbrado');

//Route::get('/lista', 'BombaController@obtenerListadoDeArchivos')->name('lista');

Route::get('/xml/{id}', 'BombaController@downloadXML')->name('xml');

Route::post('/borrar/{id}', 'BombaController@destroy')->name('borrar');

Route::get('/carga', function () {
    return view('carga');
});


Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('users', 'UserController');

Route::resource('/', 'BombaController');





Route::resource('orgs', 'orgsController');

Route::resource('facts', 'factsController');
