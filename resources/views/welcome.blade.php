<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bomberazo</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>


</head>

<nav>
    <div class="nav-wrapper black">
        <a href="{{url('/')}}" class="brand-logo">Bomberazo</a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="{{url('/')}}">Timbrado</a></li>
            <li><a href="{{url('/carga')}}">Carga excel</a></li>
        </ul>
    </div>
</nav>
<body>
<div class="row">

    <div class="col s12 m12">
        <div class="card-panel">
        <span class="blue-text text-darken-2">
            <table class="responsive-table centered">
        <thead class="text-black">
          <tr>
              <th>Name</th>
              <th>Item Name</th>
              <th>Item Price</th>
              <th>Name</th>
              <th>Item Name</th>
              <th>Item Price</th>
              <th>Name</th>
              <th>Item Name</th>
              <th>Item Price</th>
              <th>Name</th>
              <th>Item Name</th>
              <th>Item Price</th>
          </tr>
        </thead>
@for ($i = 0; $i < 15; $i++)
                    <tbody>
          <tr>

            <td>Alvin</td>
            <td>Eclair</td>
            <td>$0.87</td>
              <td>Alvin</td>
            <td>Eclair</td>
            <td>$0.87</td>
              <td>Alvin</td>
            <td>Eclair</td>
            <td>$0.87</td>
              <td>Alvin</td>
            <td>Eclair</td>
            <td>$0.87</td>

          </tr>
    @endfor
        </tbody>
      </table>
        </span>
        </div>
    </div>


</div>


<!--<div class="flex-center position-ref full-height">
            @if (Route::has('login'))
    <div class="top-right links">
@auth
        <a href="{{ url('/home') }}">Home</a>
                    @else
        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
            <a href="{{ route('register') }}">Register</a>
                        @endif
    @endauth
        </div>-->
@endif

<!--<div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://vapor.laravel.com">Vapor</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>-->
</div>
</body>

</html>
