<!-- Rfc Field -->
<div class="form-group">
    {!! Form::label('RFC', 'Rfc:') !!}
    <p>{{ $orgs->RFC }}</p>
</div>

<!-- Razon Social Field -->
<div class="form-group">
    {!! Form::label('Razon_social', 'Razon Social:') !!}
    <p>{{ $orgs->Razon_social }}</p>
</div>

<!-- Regimen Fiscal Field -->
<div class="form-group">
    {!! Form::label('Regimen_fiscal', 'Regimen Fiscal:') !!}
    <p>{{ $orgs->Regimen_fiscal }}</p>
</div>

<!-- Url Dev Field -->
<div class="form-group">
    {!! Form::label('URL_dev', 'Url Dev:') !!}
    <p>{{ $orgs->URL_dev }}</p>
</div>

<!-- Url Prod Field -->
<div class="form-group">
    {!! Form::label('URL_prod', 'Url Prod:') !!}
    <p>{{ $orgs->URL_prod }}</p>
</div>

<!-- Token Dev Field -->
<div class="form-group">
    {!! Form::label('token_dev', 'Token Dev:') !!}
    <p>{{ $orgs->token_dev }}</p>
</div>

<!-- Token Prod Field -->
<div class="form-group">
    {!! Form::label('token_prod', 'Token Prod:') !!}
    <p>{{ $orgs->token_prod }}</p>
</div>

<!-- Pass Field -->
<div class="form-group">
    {!! Form::label('pass', 'Pass:') !!}
    <p>{{ $orgs->pass }}</p>
</div>

<!-- Ruta Pem Field -->
<div class="form-group">
    {!! Form::label('ruta_pem', 'Ruta Pem:') !!}
    <p>{{ $orgs->ruta_pem }}</p>
</div>

<!-- Ruta Cer Field -->
<div class="form-group">
    {!! Form::label('ruta_cer', 'Ruta Cer:') !!}
    <p>{{ $orgs->ruta_cer }}</p>
</div>

<!-- Cp Field -->
<div class="form-group">
    {!! Form::label('cp', 'Cp:') !!}
    <p>{{ $orgs->cp }}</p>
</div>

<!-- Prod Field -->
<div class="form-group">
    {!! Form::label('Prod', 'Prod:') !!}
    <p>{{ $orgs->Prod }}</p>
</div>

<!-- Dir Field -->
<div class="form-group">
    {!! Form::label('dir', 'Dir:') !!}
    <p>{{ $orgs->dir }}</p>
</div>

