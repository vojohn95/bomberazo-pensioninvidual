<div class="table-responsive">
    <table class="table" id="orgs-table">
        <thead>
            <tr>
                <th>Rfc</th>
        <th>Razon Social</th>
        <th>Regimen Fiscal</th>
        <th>Url Dev</th>
        <th>Url Prod</th>
        <th>Token Dev</th>
        <th>Token Prod</th>
        <th>Pass</th>
        <th>Ruta Pem</th>
        <th>Ruta Cer</th>
        <th>Cp</th>
        <th>Prod</th>
        <th>Dir</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($orgs as $orgs)
            <tr>
                <td>{{ $orgs->RFC }}</td>
            <td>{{ $orgs->Razon_social }}</td>
            <td>{{ $orgs->Regimen_fiscal }}</td>
            <td>{{ $orgs->URL_dev }}</td>
            <td>{{ $orgs->URL_prod }}</td>
            <td>{{ $orgs->token_dev }}</td>
            <td>{{ $orgs->token_prod }}</td>
            <td>{{ $orgs->pass }}</td>
            <td>{{ $orgs->ruta_pem }}</td>
            <td>{{ $orgs->ruta_cer }}</td>
            <td>{{ $orgs->cp }}</td>
            <td>{{ $orgs->Prod }}</td>
            <td>{{ $orgs->dir }}</td>
                <td>
                    {!! Form::open(['route' => ['orgs.destroy', $orgs->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('orgs.show', [$orgs->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('orgs.edit', [$orgs->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
