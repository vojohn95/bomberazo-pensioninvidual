<!-- No Est Field -->
<div class="form-group">
    {!! Form::label('no_est', 'No Est:') !!}
    <p>{{ $bomba->no_est }}</p>
</div>

<!-- Num Pen Field -->
<div class="form-group">
    {!! Form::label('num_pen', 'Num Pen:') !!}
    <p>{{ $bomba->num_pen }}</p>
</div>

<!-- Rs Field -->
<div class="form-group">
    {!! Form::label('rs', 'Rs:') !!}
    <p>{{ $bomba->rs }}</p>
</div>

<!-- Rfc Field -->
<div class="form-group">
    {!! Form::label('rfc', 'Rfc:') !!}
    <p>{{ $bomba->rfc }}</p>
</div>

<!-- Clave Sat Field -->
<div class="form-group">
    {!! Form::label('clave_sat', 'Clave Sat:') !!}
    <p>{{ $bomba->clave_sat }}</p>
</div>

<!-- Clave Unidad Field -->
<div class="form-group">
    {!! Form::label('clave_unidad', 'Clave Unidad:') !!}
    <p>{{ $bomba->clave_unidad }}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{{ $bomba->descripcion }}</p>
</div>

<!-- Total Field -->
<div class="form-group">
    {!! Form::label('total', 'Total:') !!}
    <p>{{ $bomba->total }}</p>
</div>

<!-- Uso Cfdi Field -->
<div class="form-group">
    {!! Form::label('uso_cfdi', 'Uso Cfdi:') !!}
    <p>{{ $bomba->uso_cfdi }}</p>
</div>

<!-- Forma Pago Field -->
<div class="form-group">
    {!! Form::label('forma_pago', 'Forma Pago:') !!}
    <p>{{ $bomba->forma_pago }}</p>
</div>

<!-- Email User Field -->
<div class="form-group">
    {!! Form::label('email_user', 'Email User:') !!}
    <p>{{ $bomba->email_user }}</p>
</div>

<!-- Email Gerente Field -->
<div class="form-group">
    {!! Form::label('email_gerente', 'Email Gerente:') !!}
    <p>{{ $bomba->email_gerente }}</p>
</div>

<!-- Tmbrada Field -->
<div class="form-group">
    {!! Form::label('tmbrada', 'Tmbrada:') !!}
    <p>{{ $bomba->tmbrada }}</p>
</div>

