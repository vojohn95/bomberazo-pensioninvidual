<div class="row">

    <div class="col s12 m12">
        <div class="card-panel">
        <span class="blue-text text-darken-2">
            <table class="responsive-table centered">
        <thead class="text-black">
            <tr>
                <th>No Est</th>
        <th>Num Pen</th>
        <th>Rs</th>
        <th>Rfc</th>
        <th>Clave Sat</th>
        <th>Clave Unidad</th>
        <th>Descripcion</th>
        <th>Total</th>
        <th>Uso Cfdi</th>
        <th>Forma Pago</th>
        <th>Email User</th>
        <th>Email Gerente</th>
        <th>Tmbrada</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($bombas as $bomba)
            <tr>
                <td>{{ $bomba->no_est }}</td>
            <td>{{ $bomba->num_pen }}</td>
            <td>{{ $bomba->rs }}</td>
            <td>{{ $bomba->rfc }}</td>
            <td>{{ $bomba->clave_sat }}</td>
            <td>{{ $bomba->clave_unidad }}</td>
            <td>{{ $bomba->descripcion }}</td>
            <td>{{ $bomba->total }}</td>
            <td>{{ $bomba->uso_cfdi }}</td>
            <td>{{ $bomba->forma_pago }}</td>
            <td>{{ $bomba->email_user }}</td>
            <td>{{ $bomba->email_gerente }}</td>
            <td>{{ $bomba->tmbrada }}</td>
                <td>
                    {!! Form::open(['route' => ['bombas.destroy', $bomba->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('bombas.show', [$bomba->id]) }}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('bombas.edit', [$bomba->id]) }}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
         </table>
        </span>
        </div>
    </div>


</div>
