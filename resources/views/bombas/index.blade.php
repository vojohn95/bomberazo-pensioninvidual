<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pensiones</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</head>
<nav>
    <div class="nav-wrapper black">
        <a href="{{url('/')}}" class="brand-logo">Pensiones</a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="{{url('/')}}">Timbrado</a></li>
            <li><a href="{{url('/carga')}}">Carga excel</a></li>
            <li><a href="https://agsc.siat.sat.gob.mx/PTSC/ValidaRFC/index.jsf?ispublic=1" target="_blank">Validar rfc</a></li>
        </ul>
    </div>
</nav>
<body>
<div class="row">
    <div class="col s12 m12">
        <div class="card-panel">
        <span class="blue-text text-darken-2">
            @include('flash::message')
    <table class="responsive-table centered">
        <thead class="text-black">
          <tr>
              <th>Numero est</th>
              <th>numero pen</th>
              <th>rs</th>
              <th>rfc</th>
              <th>clave_sat</th>
              <th>clave_unidad</th>
              <th>descripcion</th>
              <th>total</th>
              <th>uso_cfdi</th>
              <th>forma_pago</th>
              <th>email_user</th>
              <th>email_gerentes</th>
              <th>Borrar</th>
              <th>archivos</th>
          </tr>
        </thead>
@foreach($bombas as $bomba)
      <tbody>
          <tr>
              <!--<td>{!! $bomba->id !!}</td>-->
              <td>{!! $bomba->no_est !!}</td>
              <td>{!! $bomba->num_pen !!}</td>
              <td>{!! $bomba->rs !!}</td>
              <td>{!! $bomba->rfc !!}</td>
              <td>{!! $bomba->clave_sat !!}</td>
              <td>{!! $bomba->clave_unidad !!}</td>
              <td>{!! $bomba->descripcion !!}</td>
              <td>{!! $bomba->total !!}</td>
              <td>{!! $bomba->uso_cfdi !!}</td>
              <td>{!! $bomba->forma_pago !!}</td>
              <td>{!! $bomba->email_user !!}</td>
              <td>{!! $bomba->email_gerente !!}</td>
              <td>{!! Form::open(['route' => ['borrar',$bomba->id]]) !!}
                                    {!! Form::submit('Rechazar', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}</td>
               <td><a href="{!! route('xml', [$bomba->id]) !!}"
                      class='btn-floating btn-sm btn-blue-grey'><i class="fas fa-download"></i></a></td>
          </tr>
     </tbody>

            @include('layouts.errors')
        @endforeach
            </table>
        </span>
        </div>
    </div>
    <a class="btn btn-success" href="{{url('timbrado')}}">Timbrar</a>
</div>
</body>

</html>
