<!-- No Est Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_est', 'No Est:') !!}
    {!! Form::number('no_est', null, ['class' => 'form-control']) !!}
</div>

<!-- Num Pen Field -->
<div class="form-group col-sm-6">
    {!! Form::label('num_pen', 'Num Pen:') !!}
    {!! Form::number('num_pen', null, ['class' => 'form-control']) !!}
</div>

<!-- Rs Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rs', 'Rs:') !!}
    {!! Form::text('rs', null, ['class' => 'form-control']) !!}
</div>

<!-- Rfc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rfc', 'Rfc:') !!}
    {!! Form::text('rfc', null, ['class' => 'form-control']) !!}
</div>

<!-- Clave Sat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('clave_sat', 'Clave Sat:') !!}
    {!! Form::text('clave_sat', null, ['class' => 'form-control']) !!}
</div>

<!-- Clave Unidad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('clave_unidad', 'Clave Unidad:') !!}
    {!! Form::text('clave_unidad', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Total Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total', 'Total:') !!}
    {!! Form::number('total', null, ['class' => 'form-control']) !!}
</div>

<!-- Uso Cfdi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('uso_cfdi', 'Uso Cfdi:') !!}
    {!! Form::text('uso_cfdi', null, ['class' => 'form-control']) !!}
</div>

<!-- Forma Pago Field -->
<div class="form-group col-sm-6">
    {!! Form::label('forma_pago', 'Forma Pago:') !!}
    {!! Form::text('forma_pago', null, ['class' => 'form-control']) !!}
</div>

<!-- Email User Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email_user', 'Email User:') !!}
    {!! Form::text('email_user', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Gerente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email_gerente', 'Email Gerente:') !!}
    {!! Form::text('email_gerente', null, ['class' => 'form-control']) !!}
</div>

<!-- Tmbrada Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tmbrada', 'Tmbrada:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('tmbrada', 0) !!}
        {!! Form::checkbox('tmbrada', '1', null) !!}
    </label>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('bombas.index') }}" class="btn btn-default">Cancel</a>
</div>
