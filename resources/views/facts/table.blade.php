<div class="table-responsive">
    <table class="table" id="facts-table">
        <thead>
            <tr>
                <th>Id Bomba</th>
        <th>Serie</th>
        <th>Tipo Doc</th>
        <th>Folio</th>
        <th>Fecha Timbrado</th>
        <th>Uuid</th>
        <th>Subtotal Factura</th>
        <th>Iva Factura</th>
        <th>Total Factura</th>
        <th>Xml</th>
        <th>Pdf</th>
        <th>Estatus</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($facts as $facts)
            <tr>
                <td>{{ $facts->id_bomba }}</td>
            <td>{{ $facts->serie }}</td>
            <td>{{ $facts->tipo_doc }}</td>
            <td>{{ $facts->folio }}</td>
            <td>{{ $facts->fecha_timbrado }}</td>
            <td>{{ $facts->uuid }}</td>
            <td>{{ $facts->subtotal_factura }}</td>
            <td>{{ $facts->iva_factura }}</td>
            <td>{{ $facts->total_factura }}</td>
            <td>{{ $facts->XML }}</td>
            <td>{{ $facts->PDF }}</td>
            <td>{{ $facts->estatus }}</td>
                <td>
                    {!! Form::open(['route' => ['facts.destroy', $facts->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('facts.show', [$facts->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('facts.edit', [$facts->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
