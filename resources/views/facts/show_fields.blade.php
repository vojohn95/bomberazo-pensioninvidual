<!-- Id Bomba Field -->
<div class="form-group">
    {!! Form::label('id_bomba', 'Id Bomba:') !!}
    <p>{{ $facts->id_bomba }}</p>
</div>

<!-- Serie Field -->
<div class="form-group">
    {!! Form::label('serie', 'Serie:') !!}
    <p>{{ $facts->serie }}</p>
</div>

<!-- Tipo Doc Field -->
<div class="form-group">
    {!! Form::label('tipo_doc', 'Tipo Doc:') !!}
    <p>{{ $facts->tipo_doc }}</p>
</div>

<!-- Folio Field -->
<div class="form-group">
    {!! Form::label('folio', 'Folio:') !!}
    <p>{{ $facts->folio }}</p>
</div>

<!-- Fecha Timbrado Field -->
<div class="form-group">
    {!! Form::label('fecha_timbrado', 'Fecha Timbrado:') !!}
    <p>{{ $facts->fecha_timbrado }}</p>
</div>

<!-- Uuid Field -->
<div class="form-group">
    {!! Form::label('uuid', 'Uuid:') !!}
    <p>{{ $facts->uuid }}</p>
</div>

<!-- Subtotal Factura Field -->
<div class="form-group">
    {!! Form::label('subtotal_factura', 'Subtotal Factura:') !!}
    <p>{{ $facts->subtotal_factura }}</p>
</div>

<!-- Iva Factura Field -->
<div class="form-group">
    {!! Form::label('iva_factura', 'Iva Factura:') !!}
    <p>{{ $facts->iva_factura }}</p>
</div>

<!-- Total Factura Field -->
<div class="form-group">
    {!! Form::label('total_factura', 'Total Factura:') !!}
    <p>{{ $facts->total_factura }}</p>
</div>

<!-- Xml Field -->
<div class="form-group">
    {!! Form::label('XML', 'Xml:') !!}
    <p>{{ $facts->XML }}</p>
</div>

<!-- Pdf Field -->
<div class="form-group">
    {!! Form::label('PDF', 'Pdf:') !!}
    <p>{{ $facts->PDF }}</p>
</div>

<!-- Estatus Field -->
<div class="form-group">
    {!! Form::label('estatus', 'Estatus:') !!}
    <p>{{ $facts->estatus }}</p>
</div>

