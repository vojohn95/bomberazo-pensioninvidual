<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Pensiones</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>


</head>

<nav>
    <div class="nav-wrapper black">
        <a href="{{url('/')}}" class="brand-logo">Pensiones</a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="{{url('/')}}">Timbrado</a></li>
            <li><a href="{{url('/carga')}}">Carga excel</a></li>
        </ul>
    </div>
</nav>
<body>

<div class="row">
    <div class="container">
        {!! Form::open(['route' => 'verificar', 'method' => 'post',  'enctype' => 'multipart/form-data']) !!}

        <div class="file-field input-field">
            <div class="btn">
                <span>Archivo</span>
                <input type="file" name="archivo" required>
            </div>
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text">
            </div>
        </div>

        {!! Form::submit('cargar excel', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

</body>

</html>
