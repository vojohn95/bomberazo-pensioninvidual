<html>
<meta charset='utf-8'>
<h4 style='font-color: green;' align='center';>Central Operadora de Estacionamientos SAPI S.A. de C.V.</h4>
<p align='center;'>Envía a usted el archivo XML correspondiente al Comprobante Fiscal Digital con Folio Fiscal: {{ $bomba['uuid'] }} , Serie: {{ $bomba['serie'] }} y Folio: {{ $bomba['folio'] }}. Así como su representación impresa.</p>
<p align='center;'>Este correo electrónico ha sido generado automáticamente por el Sistema de Emisión de Comprobantes Digitales por lo que le solicitamos no responder a este mensaje, ya que las respuestas a este correo electrónico no serán leídas. En caso de tener alguna duda referente a la información contenida en el Comprobante Fiscal Digital contacte a Central Operadora de Estacionamientos SAPI S.A. de C.V. para su aclaración.</p>
<p align='center;'>Está recibiendo este correo electrónico debido a que ha proporcionado la dirección de correo electrónico {{ $bomba['correo'] }} a Central Operadora de Estacionamientos SAPI S.A. de C.V. para hacerle llegar su Factura Electrónica.!</p>
<p align='center;'>Estimado usuario le recomendamos realizar sus facturas en un lapso no mayor a 5 días hábiles</p>
<p align='center;'>Contactanos</p>
<p align='center;'>Teléfono: (55) 3640-3900</p>
<p align='center;'>Correo: facturacion@central-mx.com</p>
</html>
