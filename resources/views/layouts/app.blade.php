<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Pensiones</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>


</head>

<body class="skin-blue sidebar-mini">
@if (!Auth::guest())
    <div class="wrapper">
        <!-- Main Header -->
            <!-- Header Navbar -->
            <nav>
                <div class="nav-wrapper black">
                    <a href="{{url('/bombas')}}" class="brand-logo">Pensiones</a>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <li><a href="{{url('/bombas')}}">Timbrado</a></li>
                        <li><a href="{{url('/carga')}}">Carga excel</a></li>
                    </ul>
                </div>
            </nav>

        <!-- Left side column. contains the logo and sidebar -->
        @include('layouts.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>

        <!-- Main Footer -->
        <footer class="main-footer" style="max-height: 100px;text-align: center">
            <strong>Copyright © 2016 <a href="#">Company</a>.</strong> All rights reserved.
        </footer>

    </div>
@else
    <nav>
        <div class="nav-wrapper black">
            <a href="{{url('/')}}" class="brand-logo">Pensiones</a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="{{url('/')}}">Timbrado</a></li>
                <li><a href="{{url('bombas')}}">Carga excel</a></li>
                <li> <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('bombas.create') }}">Add New</a></li>
            </ul>
        </div>
    </nav>
<body>

<div class="row">

    <div class="col s12 m12">
        <span class="blue-text text-darken-2" style="font-size: 12px;">
            <table class="responsive-table centered">
        <thead class="text-black ">
          <tr>
              <th>No Est</th>
        <th>Num Pen</th>
        <th>Rs</th>
        <th>Rfc</th>
        <th>Clave Sat</th>
        <th>Clave Unidad</th>
        <th>Descripcion</th>
        <th>Total</th>
        <th>Uso Cfdi</th>
        <th>Forma Pago</th>
        <th>Email User</th>
        <th>Email Gerente</th>
        <th>Tmbrada</th>
                <th colspan="3">Action</th>
          </tr>
        </thead>
        @foreach($bombas as $bomba)
                    <tbody>
          <tr>

           <td>{{ $bomba->no_est }}</td>
            <td>{{ $bomba->num_pen }}</td>
            <td>{{ $bomba->rs }}</td>
            <td>{{ $bomba->rfc }}</td>
            <td>{{ $bomba->clave_sat }}</td>
            <td>{{ $bomba->clave_unidad }}</td>
            <td>{{ $bomba->descripcion }}</td>
            <td>{{ $bomba->total }}</td>
            <td>{{ $bomba->uso_cfdi }}</td>
            <td>{{ $bomba->forma_pago }}</td>
            <td>{{ $bomba->email_user }}</td>
            <td>{{ $bomba->email_gerente }}</td>
            <td>{{ $bomba->tmbrada }}</td>

          </tr>
    @endforeach
        </tbody>
      </table>
        </span>
    </div>


</div>

</body>
    @endif

    <!-- jQuery 3.1.1 -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/js/adminlte.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

    @yield('scripts')
</body>
</html>
