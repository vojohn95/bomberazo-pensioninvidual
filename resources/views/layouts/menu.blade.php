<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{{ route('users.index') }}"><i class="fa fa-edit"></i><span>Users</span></a>
</li>

<li class="{{ Request::is('bombas*') ? 'active' : '' }}">
    <a href="{{ route('bombas.index') }}"><i class="fa fa-edit"></i><span>Bombas</span></a>
</li>

<li class="{{ Request::is('orgs*') ? 'active' : '' }}">
    <a href="{{ route('orgs.index') }}"><i class="fa fa-edit"></i><span>Orgs</span></a>
</li>

<li class="{{ Request::is('facts*') ? 'active' : '' }}">
    <a href="{{ route('facts.index') }}"><i class="fa fa-edit"></i><span>Facts</span></a>
</li>

