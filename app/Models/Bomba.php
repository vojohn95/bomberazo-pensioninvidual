<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Bomba
 * @package App\Models
 * @version December 11, 2019, 3:06 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection facts
 * @property integer no_est
 * @property integer num_pen
 * @property string rs
 * @property string rfc
 * @property string clave_sat
 * @property string clave_unidad
 * @property string descripcion
 * @property number total
 * @property string uso_cfdi
 * @property string forma_pago
 * @property string email_user
 * @property string email_gerente
 * @property boolean tmbrada
 */
class Bomba extends Model
{

    public $table = 'bombas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'no_est',
        'num_pen',
        'rs',
        'rfc',
        'clave_sat',
        'clave_unidad',
        'descripcion',
        'total',
        'uso_cfdi',
        'forma_pago',
        'email_user',
        'email_gerente',
        'tmbrada',
        'folio'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'no_est' => 'string',
        'num_pen' => 'integer',
        'folio' => 'integer',
        'rs' => 'string',
        'rfc' => 'string',
        'clave_sat' => 'string',
        'clave_unidad' => 'string',
        'descripcion' => 'string',
        'total' => 'float',
        'uso_cfdi' => 'string',
        'forma_pago' => 'string',
        'email_user' => 'string',
        'email_gerente' => 'string',
        'tmbrada' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'no_est' => 'required',
        'num_pen' => 'required',
        'rs' => 'required',
        'rfc' => 'required',
        'clave_sat' => 'required',
        'clave_unidad' => 'required',
        'descripcion' => 'required',
        'total' => 'required',
        'uso_cfdi' => 'required',
        'forma_pago' => 'required',
        'email_user' => 'required',
        'email_gerente' => 'required',
        'tmbrada' => 'required',
        'folio' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function facts()
    {
        return $this->hasMany(\App\Models\Fact::class, 'id_bomba');
    }
}
