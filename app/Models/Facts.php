<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Facts
 * @package App\Models
 * @version December 11, 2019, 6:02 pm UTC
 *
 * @property \App\Models\Bomba idBomba
 * @property integer id_bomba
 * @property string serie
 * @property string tipo_doc
 * @property string folio
 * @property string|\Carbon\Carbon fecha_timbrado
 * @property string uuid
 * @property number subtotal_factura
 * @property number iva_factura
 * @property number total_factura
 * @property string XML
 * @property string PDF
 * @property string estatus
 */
class Facts extends Model
{
    use SoftDeletes;

    public $table = 'facts';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_bomba',
        'serie',
        'tipo_doc',
        'folio',
        'fecha_timbrado',
        'uuid',
        'subtotal_factura',
        'iva_factura',
        'total_factura',
        'XML',
        'PDF',
        'estatus'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_bomba' => 'integer',
        'serie' => 'string',
        'tipo_doc' => 'string',
        'folio' => 'string',
        'fecha_timbrado' => 'datetime',
        'uuid' => 'string',
        'subtotal_factura' => 'float',
        'iva_factura' => 'float',
        'total_factura' => 'float',
        'XML' => 'string',
        'PDF' => 'string',
        'estatus' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_bomba' => 'required',
        'serie' => 'required',
        'tipo_doc' => 'required',
        'folio' => 'required',
        'subtotal_factura' => 'required',
        'iva_factura' => 'required',
        'total_factura' => 'required',
        'XML' => 'required',
        'PDF' => 'required',
        'estatus' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idBomba()
    {
        return $this->belongsTo(\App\Models\Bomba::class, 'id_bomba');
    }
}
