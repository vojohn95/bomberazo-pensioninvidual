<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Orgs
 * @package App\Models
 * @version December 11, 2019, 6:02 pm UTC
 *
 * @property string RFC
 * @property string Razon_social
 * @property string Regimen_fiscal
 * @property string URL_dev
 * @property string URL_prod
 * @property string token_dev
 * @property string token_prod
 * @property string pass
 * @property string ruta_pem
 * @property string ruta_cer
 * @property string cp
 * @property boolean Prod
 * @property string dir
 */
class Orgs extends Model
{
    use SoftDeletes;

    public $table = 'orgs';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'RFC',
        'Razon_social',
        'Regimen_fiscal',
        'URL_dev',
        'URL_prod',
        'token_dev',
        'token_prod',
        'pass',
        'ruta_pem',
        'ruta_cer',
        'cp',
        'Prod',
        'dir'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'RFC' => 'string',
        'Razon_social' => 'string',
        'Regimen_fiscal' => 'string',
        'URL_dev' => 'string',
        'URL_prod' => 'string',
        'token_dev' => 'string',
        'token_prod' => 'string',
        'pass' => 'string',
        'ruta_pem' => 'string',
        'ruta_cer' => 'string',
        'cp' => 'string',
        'Prod' => 'boolean',
        'dir' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'RFC' => 'required',
        'Razon_social' => 'required',
        'Regimen_fiscal' => 'required',
        'URL_dev' => 'required',
        'URL_prod' => 'required',
        'token_dev' => 'required',
        'token_prod' => 'required',
        'pass' => 'required',
        'ruta_pem' => 'required',
        'ruta_cer' => 'required',
        'cp' => 'required',
        'Prod' => 'required',
        'dir' => 'required'
    ];

    
}
