<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFactsRequest;
use App\Http\Requests\UpdateFactsRequest;
use App\Repositories\FactsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class FactsController extends AppBaseController
{
    /** @var  FactsRepository */
    private $factsRepository;

    public function __construct(FactsRepository $factsRepo)
    {
        $this->factsRepository = $factsRepo;
    }

    /**
     * Display a listing of the Facts.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $facts = $this->factsRepository->all();

        return view('facts.index')
            ->with('facts', $facts);
    }

    /**
     * Show the form for creating a new Facts.
     *
     * @return Response
     */
    public function create()
    {
        return view('facts.create');
    }

    /**
     * Store a newly created Facts in storage.
     *
     * @param CreateFactsRequest $request
     *
     * @return Response
     */
    public function store(CreateFactsRequest $request)
    {
        $input = $request->all();

        $facts = $this->factsRepository->create($input);

        Flash::success('Facts saved successfully.');

        return redirect(route('facts.index'));
    }

    /**
     * Display the specified Facts.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $facts = $this->factsRepository->find($id);

        if (empty($facts)) {
            Flash::error('Facts not found');

            return redirect(route('facts.index'));
        }

        return view('facts.show')->with('facts', $facts);
    }

    /**
     * Show the form for editing the specified Facts.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $facts = $this->factsRepository->find($id);

        if (empty($facts)) {
            Flash::error('Facts not found');

            return redirect(route('facts.index'));
        }

        return view('facts.edit')->with('facts', $facts);
    }

    /**
     * Update the specified Facts in storage.
     *
     * @param int $id
     * @param UpdateFactsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFactsRequest $request)
    {
        $facts = $this->factsRepository->find($id);

        if (empty($facts)) {
            Flash::error('Facts not found');

            return redirect(route('facts.index'));
        }

        $facts = $this->factsRepository->update($request->all(), $id);

        Flash::success('Facts updated successfully.');

        return redirect(route('facts.index'));
    }

    /**
     * Remove the specified Facts from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $facts = $this->factsRepository->find($id);

        if (empty($facts)) {
            Flash::error('Facts not found');

            return redirect(route('facts.index'));
        }

        $this->factsRepository->delete($id);

        Flash::success('Facts deleted successfully.');

        return redirect(route('facts.index'));
    }
}
