<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrgsRequest;
use App\Http\Requests\UpdateOrgsRequest;
use App\Repositories\OrgsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class OrgsController extends AppBaseController
{
    /** @var  OrgsRepository */
    private $orgsRepository;

    public function __construct(OrgsRepository $orgsRepo)
    {
        $this->orgsRepository = $orgsRepo;
    }

    /**
     * Display a listing of the Orgs.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $orgs = $this->orgsRepository->all();

        return view('orgs.index')
            ->with('orgs', $orgs);
    }

    /**
     * Show the form for creating a new Orgs.
     *
     * @return Response
     */
    public function create()
    {
        return view('orgs.create');
    }

    /**
     * Store a newly created Orgs in storage.
     *
     * @param CreateOrgsRequest $request
     *
     * @return Response
     */
    public function store(CreateOrgsRequest $request)
    {
        $input = $request->all();

        $orgs = $this->orgsRepository->create($input);

        Flash::success('Orgs saved successfully.');

        return redirect(route('orgs.index'));
    }

    /**
     * Display the specified Orgs.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $orgs = $this->orgsRepository->find($id);

        if (empty($orgs)) {
            Flash::error('Orgs not found');

            return redirect(route('orgs.index'));
        }

        return view('orgs.show')->with('orgs', $orgs);
    }

    /**
     * Show the form for editing the specified Orgs.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $orgs = $this->orgsRepository->find($id);

        if (empty($orgs)) {
            Flash::error('Orgs not found');

            return redirect(route('orgs.index'));
        }

        return view('orgs.edit')->with('orgs', $orgs);
    }

    /**
     * Update the specified Orgs in storage.
     *
     * @param int $id
     * @param UpdateOrgsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrgsRequest $request)
    {
        $orgs = $this->orgsRepository->find($id);

        if (empty($orgs)) {
            Flash::error('Orgs not found');

            return redirect(route('orgs.index'));
        }

        $orgs = $this->orgsRepository->update($request->all(), $id);

        Flash::success('Orgs updated successfully.');

        return redirect(route('orgs.index'));
    }

    /**
     * Remove the specified Orgs from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $orgs = $this->orgsRepository->find($id);

        if (empty($orgs)) {
            Flash::error('Orgs not found');

            return redirect(route('orgs.index'));
        }

        $this->orgsRepository->delete($id);

        Flash::success('Orgs deleted successfully.');

        return redirect(route('orgs.index'));
    }
}
