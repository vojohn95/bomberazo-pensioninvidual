<?php

namespace App\Http\Controllers;

use App\Funciones\Timbrar;
use App\Http\Requests\CreateBombaRequest;
use App\Http\Requests\UpdateBombaRequest;
use App\Imports\BombaImport;
use App\Models\Bomba;
use App\Models\Facts;
use App\Quotation;
use App\Repositories\BombaRepository;
use Illuminate\Support\Facades\DB;
use Flash;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Response;

class BombaController extends AppBaseController
{
    /** @var  BombaRepository */
    private $bombaRepository;

    public function __construct(BombaRepository $bombaRepo)
    {
        $this->bombaRepository = $bombaRepo;
    }

    /**
     * Display a listing of the Bomba.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index()
    {
        //$bombas = $this->bombaRepository->all();
        $bombas = DB::table('bombas')
            ->where('tmbrada', False)
            ->select('id', 'folio', 'no_est', 'num_pen', 'rs', 'rfc', 'clave_sat', 'clave_unidad', 'descripcion', 'total', 'uso_cfdi', 'forma_pago', 'email_user', 'email_gerente',
                DB::raw('substr(uso_cfdi, 1, 3)'), DB::raw('substr(lpad(forma_pago,2 , 0),1, 2)'), 'email_user', 'email_gerente')->get();

        return view('bombas.index')
            ->with('bombas', $bombas);
    }

    /**
     * Show the form for creating a new Bomba.
     *
     * @return Response
     */
    public function create()
    {
        return view('bombas.create');
    }

    /**
     * Store a newly created Bomba in storage.
     *
     * @param CreateBombaRequest $request
     *
     * @return Response
     */
    public function store()
    {
        /*$input = $request->all();

        $bomba = $this->bombaRepository->create($input);

        Flash::success('Bomba saved successfully.');

        return redirect(route('bombas.index'));*/
    }

    public function Carga()
    {
        Excel::import(new BombaImport, request()->file('archivo'));
        Flash::success('Excel cargado');
        return redirect(route('bombas.index'));
    }

    public function timbrado()
    {
        $bombas = Bomba::where('tmbrada', False)->get();
        foreach ($bombas as $ticket) {
            $timbrar = new Timbrar();
            $timbrar->Factura($ticket);
        }
        return redirect(route('bombas.index'));
    }

    /**
     * Display the specified Bomba.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $bomba = $this->bombaRepository->find($id);

        if (empty($bomba)) {
            Flash::error('Bomba not found');

            return redirect(route('bombas.index'));
        }

        return view('bombas.show')->with('bomba', $bomba);
    }

    /**
     * Show the form for editing the specified Bomba.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $bomba = $this->bombaRepository->find($id);

        if (empty($bomba)) {
            Flash::error('Bomba not found');

            return redirect(route('bombas.index'));
        }

        return view('bombas.edit')->with('bomba', $bomba);
    }

    /**
     * Update the specified Bomba in storage.
     *
     * @param int $id
     * @param UpdateBombaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBombaRequest $request)
    {
        $bomba = $this->bombaRepository->find($id);

        if (empty($bomba)) {
            Flash::error('Bomba not found');

            return redirect(route('bombas.index'));
        }

        $bomba = $this->bombaRepository->update($request->all(), $id);

        Flash::success('Bomba updated successfully.');

        return redirect(route('bombas.index'));
    }

    /**
     * Remove the specified Bomba from storage.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        // $id = \request()->all();

        /*$user = Bomba::find($id);
        dd($user);
        if($user){
            $destroy = Bomba::destroy(2);
            dd("Se elimino");
        }*/
        $bomba = Bomba::find($id);
        $bomba->delete();
        //dd($bomba);

        /*if (empty($bomba)) {
            Flash::error('Bomba not found');

            return redirect(route('bombas.index'));
        }*/

        //$this->bombaRepository->delete($id);

        Flash::success('Registro eliminada: ' . $id);

        return redirect(route('bombas.index'));
    }

    function obtenerListadoDeArchivos()
    {
        $directorio = "/";
        // Array en el que obtendremos los resultados
        $res = array();

        // Agregamos la barra invertida al final en caso de que no exista
        if (substr($directorio, -1) != "Facturas/") $directorio .= "Facturas/";

        // Creamos un puntero al directorio y obtenemos el listado de archivos
        $dir = @dir($directorio) or die("getFileList: Error abriendo el directorio $directorio para leerlo");
        while (($archivo = $dir->read()) !== false) {
            // Obviamos los archivos ocultos
            if ($archivo[0] == ".") continue;
            if (is_dir($directorio . $archivo)) {
                $res[] = array(
                    "Nombre" => $directorio . $archivo . "/",
                    "Tamaño" => 0,
                    "Modificado" => filemtime($directorio . $archivo)
                );
            } else if (is_readable($directorio . $archivo)) {
                $res[] = array(
                    "Nombre" => $directorio . $archivo,
                    "Tamaño" => filesize($directorio . $archivo),
                    "Modificado" => filemtime($directorio . $archivo)
                );
            }
        }
        $dir->close();
        return $res;
    }

    public function downloadXML($id)
    {
        //$id = \request()->request;
        //dd($id);
        $xml = Facts::find($id);
        $archivo = $xml->XML;
        $archivo2 = $xml->PDF;
        $uuid = $xml->uuid;
        $path = "xml_down/" . $uuid . ".xml";
        $decoded = base64_decode($archivo2);
        $path2 = "xml_down/" . $uuid . ".pdf";
        file_put_contents($path2, $decoded);
        file_put_contents($path, $archivo);
        $zipname = $uuid . 'zip';
        $zip = new ZipArchive;
        $zip->open($zipname, ZipArchive::CREATE);
        $zip->addFile($path);
        $zip->addFile($path2);
        $zip->close();
        header('Content-Type: application/zip');
        header('Content-disposition: attachment; filename=' . $zipname);
        header('Content-Length: ' . filesize($zipname));
        readfile($zipname);


        //Flash::success('Factura descargada');

        //return redirect(route('facturas.index'));
    }

    public function downloadPDF($id)
    {
        //$id = \request()->request;
        //dd($id);
        $pdf = Facts::find($id);
        $archivo2 = $pdf->PDF;
        $uuid = $pdf->uuid;

        $decoded = base64_decode($archivo);
        $path = "xml_down/" . $uuid . ".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=$uuid.pdf");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);

        //Flash::success('Factura descargada');

        //return redirect(route('facturas.index'));
    }

}
