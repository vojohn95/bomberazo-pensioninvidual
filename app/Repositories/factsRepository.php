<?php

namespace App\Repositories;

use App\Models\Facts;
use App\Repositories\BaseRepository;

/**
 * Class FactsRepository
 * @package App\Repositories
 * @version December 11, 2019, 6:02 pm UTC
*/

class FactsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_bomba',
        'serie',
        'tipo_doc',
        'folio',
        'fecha_timbrado',
        'uuid',
        'subtotal_factura',
        'iva_factura',
        'total_factura',
        'XML',
        'PDF',
        'estatus'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Facts::class;
    }
}
