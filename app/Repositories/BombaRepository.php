<?php

namespace App\Repositories;

use App\Models\Bomba;
use App\Repositories\BaseRepository;

/**
 * Class BombaRepository
 * @package App\Repositories
 * @version December 11, 2019, 3:06 am UTC
*/

class BombaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'no_est',
        'num_pen',
        'rs',
        'rfc',
        'clave_sat',
        'clave_unidad',
        'descripcion',
        'total',
        'uso_cfdi',
        'forma_pago',
        'email_user',
        'email_gerente',
        'tmbrada'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Bomba::class;
    }
}
