<?php

namespace App\Repositories;

use App\Models\Orgs;
use App\Repositories\BaseRepository;

/**
 * Class OrgsRepository
 * @package App\Repositories
 * @version December 11, 2019, 6:02 pm UTC
*/

class OrgsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'RFC',
        'Razon_social',
        'Regimen_fiscal',
        'URL_dev',
        'URL_prod',
        'token_dev',
        'token_prod',
        'pass',
        'ruta_pem',
        'ruta_cer',
        'cp',
        'Prod',
        'dir'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Orgs::class;
    }
}
