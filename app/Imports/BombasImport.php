<?php

namespace App\Imports;

use App\Models\Bomba;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class BombaImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        if (!isset($row['numero_de_estacionamiento'])
            || !isset($row['numero_de_pension'])
            || !isset($row['razon_social'])
            || !isset($row['rfc'])
            || !isset($row['clave_sat'])
            || !isset($row['clave_de_unidad'])
            || !isset($row['descripcion'])
            || !isset($row['total_pago'])
            || !isset($row['uso_de_cfdi'])
            || !isset($row['forma_de_pago_clave'])
            || !isset($row['correo_usuario'])
            || !isset($row['correo_gerente'])
        ) {
            return null;
        }
        $bomba = new Bomba([
            'no_est' => $row['numero_de_estacionamiento'],
            'num_pen' => $row['numero_de_pension'],
            'rs' => $row['razon_social'],
            'rfc' => $row['rfc'],
            'clave_sat' => $row['clave_sat'],
            'clave_unidad' => $row['clave_de_unidad'],
            'descripcion' => $row['descripcion'],
            'total' => $row['total_pago'],
            'uso_cfdi' => $row['uso_de_cfdi'],
            'forma_pago' => $row['forma_de_pago_clave'],
            'email_user' => $row['correo_usuario'],
            'email_gerente' => $row['correo_gerente'],
            'tmbrada' => false,
            'folio' => $row['folio']
        ]);
        $bomba->save();
        return $bomba;
    }
}
