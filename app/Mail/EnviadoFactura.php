<?php

namespace App\Mail;

use App\Models\Bomba;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EnviadoFactura extends Mailable
{
    use Queueable, SerializesModels;

    public $bomba = array();

    public function __construct($bomba)
    {
        $this->$bomba = $bomba;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->view('email.email');
        $email->attach("Facturas/" . $bomba['uuid'] . '.pdf');
        $email->attach("Facturas/" . $bomba['uuid'] . '.xml');
        return $email;
    }
}
