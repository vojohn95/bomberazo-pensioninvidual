<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Bombas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bombas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_est');
            $table->integer('num_pen');
            $table->integer('folio');
            $table->string('rs');
            $table->string('rfc');
            $table->string('clave_sat');
            $table->string('clave_unidad');
            $table->string('descripcion');
            $table->float('total');
            $table->string('uso_cfdi');
            $table->string('forma_pago');
            $table->string('email_user');
            $table->string('email_gerente');
            $table->boolean('tmbrada');
            $table->timestamps();
        });

        Schema::create('facts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_bomba');
            $table->foreign('id_bomba')
                ->references('id')
                ->on('bombas')
                ->onDelete('cascade');
            $table->string('serie');
            $table->string('tipo_doc');
            $table->string('folio');
            $table->dateTime('fecha_timbrado')->nullable();
            $table->string('uuid')->nullable();
            $table->decimal('subtotal_factura');
            $table->decimal('iva_factura');
            $table->decimal('total_factura');
            $table->longText('XML');
            $table->longText('PDF');
            $table->string('estatus');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bombas');
    }
}
