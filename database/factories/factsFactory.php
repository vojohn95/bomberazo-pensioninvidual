<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Facts;
use Faker\Generator as Faker;

$factory->define(Facts::class, function (Faker $faker) {

    return [
        'id_bomba' => $faker->word,
        'serie' => $faker->word,
        'tipo_doc' => $faker->word,
        'folio' => $faker->word,
        'fecha_timbrado' => $faker->date('Y-m-d H:i:s'),
        'uuid' => $faker->word,
        'subtotal_factura' => $faker->word,
        'iva_factura' => $faker->word,
        'total_factura' => $faker->word,
        'XML' => $faker->text,
        'PDF' => $faker->text,
        'estatus' => $faker->word,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
