<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Bomba;
use Faker\Generator as Faker;

$factory->define(Bomba::class, function (Faker $faker) {

    return [
        'no_est' => $faker->randomDigitNotNull,
        'num_pen' => $faker->randomDigitNotNull,
        'rs' => $faker->word,
        'rfc' => $faker->word,
        'clave_sat' => $faker->word,
        'clave_unidad' => $faker->word,
        'descripcion' => $faker->word,
        'total' => $faker->randomDigitNotNull,
        'uso_cfdi' => $faker->word,
        'forma_pago' => $faker->word,
        'email_user' => $faker->word,
        'email_gerente' => $faker->word,
        'tmbrada' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
